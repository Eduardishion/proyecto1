import React, { Component } from "react"
import Header from "./Header"
import Formulario from "./Formulario"
import Resultado from './Resultado'
import Resumen from './Resumen'
import "../css/App.css"

import {obtenerDiferenciaAnio,calcularMarca,obtenerPlan} from '../helper'

export default class App extends Component {

	state = {
		resultado: '',
		datos: {}
	}

	cotizarSeguro = (datos) => {
		const {marca,plan,year} = datos
		let resultado = 2000

		const diferencia = obtenerDiferenciaAnio(year)

		resultado -= ((diferencia * 3) * resultado)/100

		resultado *= calcularMarca(marca)

		resultado *= obtenerPlan(plan)

		resultado = parseFloat(resultado).toFixed(2)

		this.setState({ resultado, datos})
	}

	render() {
		return (
			<div className="contenedor">
				<Header
					titulo='Cotizador de Seguros de Auto'/>

				<div className="contenedor-formulario">

					<Formulario
						cotizarSeguro={this.cotizarSeguro}
					/>

					<Resumen
						datos={this.state.datos}
					/>

					<Resultado
						resultado={this.state.resultado}
					/>
				</div>
			</div>
		)
	}
}
